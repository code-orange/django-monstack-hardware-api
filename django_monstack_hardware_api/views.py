from rest_framework import permissions, status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from django_cdstack_deploy.django_cdstack_deploy.models import *
from django_cdstack_models.django_cdstack_models.func import get_or_create_cmdb_host


@api_view(["PUT"])
@permission_classes((permissions.AllowAny,))
def hardware(request):
    try:
        tmp_int = int(request.data.get("deployment_id"))
    except ValueError:
        content = {"error": "Instance-ID not found or incorrect!"}
        return Response(content, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

    if request.method == "PUT":
        try:
            instance = CmdbInstance.objects.get(id=request.data.get("deployment_id"))
        except CmdbInstance.DoesNotExist:
            content = {"error": "Instance-ID not found or incorrect!"}
            return Response(content, status=status.HTTP_404_NOT_FOUND)

        try:
            api_key = instance.api_key
        except CmdbApiKeyInstance.DoesNotExist:
            content = {"error": "Instance-ApiKey not found or incorrect!"}
            return Response(content, status=status.HTTP_404_NOT_FOUND)

        if not api_key.key == request.data.get("deployment_pw"):
            content = {"error": "Instance-ApiKey not found or incorrect!"}
            return Response(content, status=status.HTTP_404_NOT_FOUND)

        if len(request.data.get("fqdn")) <= 3:
            content = {"error": "Hostname invalid / too short!"}
            return Response(content, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        host = get_or_create_cmdb_host(request.data.get("fqdn"), instance)

        hardware_report = request.data.get("hardware_report")

        try:
            info_dump_cpuz_report_html = host.cmdbvarshost_set.get(
                name="info_dump_cpuz_report_html"
            )
        except CmdbVarsHost.DoesNotExist:
            info_dump_cpuz_report_html = CmdbVarsHost(
                host_rel=host, name="info_dump_cpuz_report_html", value=""
            )
            info_dump_cpuz_report_html.save()

        info_dump_cpuz_report_html.value = hardware_report
        info_dump_cpuz_report_html.save()

    else:
        content = "SOMETHING BROKE BADLY"
        return Response(content, status=status.HTTP_501_NOT_IMPLEMENTED)

    content = "OK"

    return Response(content, status=status.HTTP_200_OK)
