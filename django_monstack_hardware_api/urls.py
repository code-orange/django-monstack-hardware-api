from django.urls import path

from . import views

urlpatterns = [
    path("hardware", views.hardware),
]
